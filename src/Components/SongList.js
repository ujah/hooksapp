import React,{useState,useEffect} from 'react';
import NewSongForm from './NewSongForm';

const SongList = () => {
    const [songs, setsongs] = useState([
        {title:'Gbese by Qdot', id:1},
        {title:'Joro by Wizkid ', id:2},
        {title:'Gum Body by Burna Boy', id:3}
    ]);

    const [age, setage] = useState(20);

    const addSong = (title) => {
        setsongs([...songs, {title: title , id: 4 }]);
    }

    useEffect(()=>{
        console.log('ran');
    }, [songs])

    useEffect(()=>{
        console.log('ran');
    }, [age])

    return (
        <div>
        <ul>
            {songs.map(song => {
                return(
                    <li key={song.id}>{song.title} </li>
                );
            })}
        </ul>
        <NewSongForm addSong={addSong}/>
        <button onClick={()=>setage(age + 1)}>Add to Age: {age}</button>
        </div>
    )
}

export default SongList;