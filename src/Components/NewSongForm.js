import React,{useState} from 'react';

const NewSongForm = ({addSong}) => {
    const [Title, setTitle] = useState('');

    const handleSubmit = (e)=>{
        e.preventDefault();
        addSong(Title);
        setTitle('');
    }

    return ( 
        <form onSubmit={handleSubmit}>
            <label> Song Name:</label>
            <input type='text' value={Title} required onChange={ (e) => setTitle(e.target.value)}/>
            <input type='submit' value='add Song'/>
        </form>
     );
}
 
export default NewSongForm;